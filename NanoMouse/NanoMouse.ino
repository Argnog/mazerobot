//#define DEBUG

#include <Servo.h>
#include "NanoMouseMotors.h"
#include "NanoMouseSensors.h"

const byte ledPin = 13;
const byte buttonPin = 9;

int targetFront;
int thresholdFront;

int targetSide;
int thresholdSide;

NanoMouseMotors motors;
//<leftEmitter,leftDetector......
NanoMouseSensors<4, A7, 3, A6, 2, A5> sensors;

void setup()
{

  motors.attach(6, 5);

  pinMode(ledPin, OUTPUT);
  pinMode(buttonPin, INPUT_PULLUP);

  sensors.configure();

  Serial.begin(9600);

  motors.stop();

  while (digitalRead(buttonPin))
  {
  }

  delay(500);

  calibrate();

}

void loop()
{
  navigateLabyrinth(state());
}

void calibrate()
{
  sensors.initialize();
  targetSide = (sensors.left + sensors.right) / 2;

  motors.turn(RIGHT, 90);
  sensors.initialize();
  targetFront = sensors.front;
  thresholdSide = (targetSide + sensors.left) / 2;

  motors.turn(LEFT, 90);
  sensors.initialize();
  thresholdFront = (targetFront + sensors.front) / 2;

#ifdef DEBUG
  Serial.print("Front threshold: ");
  Serial.println(thresholdFront);
  Serial.print("Front target: ");
  Serial.println(targetFront);
  Serial.print("Threshold Side: ");
  Serial.println(thresholdSide);
  Serial.print("Side Target: ");
  Serial.println(targetSide);
#endif
}

void forwardWhiskers()
{
  while (sensors.front < targetFront * .4)
  {
    sensors.sense();


    if (sensors.left > thresholdSide && sensors.right > thresholdSide)
      motors.forwardProportional(sensors.right - sensors.left);
    else if (sensors.right > thresholdSide)
      motors.forwardProportional(sensors.right - targetSide);
    else if (sensors.left > thresholdSide)
      motors.forwardProportional(targetSide - sensors.left);
    else
      motors.forward();
  }
  motors.stop();

}

byte state()
{
  int threshold = 30;
  byte event = 0;

  sensors.sense();

  if (sensors.front > thresholdFront)
    event += 1;

  if (sensors.left > thresholdSide)
    event += 2;

  if (sensors.right > thresholdSide)
    event += 4;


  return event;

}


void avoid(byte event)
{
  switch (event)
  {
    case 1: //Front sensor triggered
      if (random(2))
      {
        motors.turn(LEFT, random(90, 181));
      }
      else
      {
        motors.turn(RIGHT, random(90, 181));
      }
      sensors.initialize();
      break;
    case 2: //Left sensor is triggered
      motors.turn(RIGHT, random(30, 61));
      sensors.initialize();
      break;
    case 4: //Right sensor is triggered
      motors.turn(LEFT, random(30, 61));
      sensors.initialize();
      break;
    default:
      motors.forward();
      break;
  }
}

void navigateLabyrinth(byte event)
{
  switch (event)
  {
    case 0:
#ifdef DEBUG
      Serial.println("Case: 0");
#endif
      digitalWrite(ledPin, HIGH);
      delay(100);
      digitalWrite(ledPin, LOW);
      delay(100);
      break;
    case 1:
    #ifdef DEBUG
      Serial.println("Case: 1");
      #endif
      digitalWrite(ledPin, HIGH);
      delay(100);
      digitalWrite(ledPin, LOW);
      delay(100);
      break;
    case 2:
    #ifdef DEBUG
      Serial.println("Case: 2");
      #endif
      forwardWhiskers();
      break;
    case 3:
    #ifdef DEBUG
      Serial.println("Case: 3");
      #endif
      motors.turn(RIGHT, 90);
      sensors.initialize();
      break;
    case 4:
    #ifdef DEBUG
      Serial.println("Case: 4");
      #endif
      forwardWhiskers();
      break;
    case 5:
    #ifdef DEBUG
      Serial.println("Case: 5");
      #endif
      motors.turn(LEFT, 90);
      sensors.initialize();
      break;
    case 6:
    #ifdef DEBUG
      Serial.println("Case: 6");
      #endif
      forwardWhiskers();
      break;
    case 7:
    #ifdef DEBUG
      Serial.println("Case: 7");
      #endif
      digitalWrite(ledPin, HIGH);
      break;

  }

}





