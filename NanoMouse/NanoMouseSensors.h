#include <arduino.h>

template <byte leftEmitter, byte leftDetector, byte frontEmitter, byte frontDetector, byte rightEmitter, byte rightDetector>

class NanoMouseSensors
{
  private:
    //used to store sensor values when the emitters are off
    int leftAmbient;
    //used to store sensor values when the emitter is on
    int leftCombined;
    //used to filter out ambieant light
    int leftReflected;

    //used to store sensor values when the emitters are off
    int frontAmbient;
    //used to store sensor values when the emitter is on
    int frontCombined;
    //used to filter out ambieant light
    int frontReflected;

    //used to store sensor values when the emitters are off
    int rightAmbient;
    //used to store sensor values when the emitter is on
    int rightCombined;
    //used to filter out ambieant light
    int rightReflected;

    int leftTotal;
    int frontTotal;
    int rightTotal;
    static const byte numReadings = 20;
    byte index;
    int leftReadings[numReadings];
    int leftSmoothed;
    int frontReadings[numReadings];
    int frontSmoothed;
    int rightReadings[numReadings];
    int rightSmoothed;

  public:
    int front;
    int left;
    int right;

    void configure()
    {
      pinMode(leftEmitter, OUTPUT);
      pinMode(frontEmitter, OUTPUT);
      pinMode(rightEmitter, OUTPUT);

    }

    void sense()
    {
      digitalWrite(leftEmitter, HIGH);
      digitalWrite(frontEmitter, HIGH);
      digitalWrite(rightEmitter, HIGH);
      delay(1);
      leftCombined = analogRead(leftDetector);
      frontCombined = analogRead(frontDetector);
      rightCombined = analogRead(rightDetector);
      digitalWrite(leftEmitter, LOW);
      digitalWrite(frontEmitter, LOW);
      digitalWrite(rightEmitter, LOW);
      delay(1);

      leftAmbient = analogRead(leftDetector);
      leftReflected = leftCombined - leftAmbient;
      leftTotal -= leftReadings[index];
      leftReadings[index] = leftReflected;
      leftTotal += leftReadings[index];

      frontAmbient = analogRead(frontDetector);
      frontReflected = frontCombined - frontAmbient;
      frontTotal -= frontReadings[index];
      frontReadings[index] = frontReflected;
      frontTotal += frontReadings[index];

      rightAmbient = analogRead(rightDetector);
      rightReflected = rightCombined - rightAmbient;
      rightTotal -= rightReadings[index];
      rightReadings[index] = rightReflected;
      rightTotal += rightReadings[index];

      index += 1;
      if (index >= numReadings)
      {
        index = 0;
      }

      leftSmoothed = leftTotal / numReadings;
      frontSmoothed = frontTotal / numReadings;
      rightSmoothed = rightTotal / numReadings;

      left = leftSmoothed;
      front = frontSmoothed;
      right = rightSmoothed;

    }



    void view()
    {
      Serial.print(left);
      Serial.print("/t");
      Serial.print(front);
      Serial.print("/t");
      Serial.println(right);  
    }

    void initialize()
    {

      for (byte i = 0; i < numReadings; i++)
      {
        sense();
      }
    }

};

