#include <Servo.h>

Servo leftServo;
Servo rightServo;

const byte ledPin = 13;

void setup()
{
  leftServo.attach(6);
  rightServo.attach(5);
  pinMode(ledPin,OUTPUT);
  Serial.begin(9600);

}

void loop()
{
  bool msgReceived = false;
  char msg[3];

  while (!msgReceived)
  {
    while (Serial.available())

    {

      Serial.readBytes(msg, 3);
      msgReceived = true;

    }

  }

  leftServo.writeMicroseconds(1500-msg[0]*5);
  rightServo.writeMicroseconds(1500+msg[1]*5);
  digitalWrite(13,msg[2]);
}